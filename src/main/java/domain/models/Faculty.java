package domain.models;

public enum Faculty {
    IT,
    ITM,
    MEDIA,
    TS,
    INDUSTRIAL
}
